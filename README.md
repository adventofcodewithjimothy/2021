# 2021 Advent of Code

A collection of simple C# projects to hold all of the 2021 Advent of Code challenges.

To get your answer, just replace the contents of `inputs/input` with your input from the puzzle.

All of the puzzles can be found [here](https://adventofcode.com/2021)