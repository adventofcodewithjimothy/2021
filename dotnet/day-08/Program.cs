﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day08
{
    class Program
    {
        static void Main(string[] args)
        {
            var single = System.IO.File.ReadAllLines("inputs/single");
            var testInput = System.IO.File.ReadAllLines("inputs/test");
            var input = System.IO.File.ReadAllLines("inputs/input");

            Console.WriteLine($"Stage 1: Test Input {FirstMethod(testInput)}");
            Console.WriteLine($"Stage 1: Real Input {FirstMethod(input)}");
            Console.WriteLine($"Stage 2: Single Input {SecondMethod(single)}");
            Console.WriteLine($"Stage 2: Test Input {SecondMethod(testInput)}");
            Console.WriteLine($"Stage 2: Real Input {SecondMethod(input)}");
        }

        static int FirstMethod(string[] input)
        {
            var digitCount = 0;
            var segmentCounts = new List<int>{2,3,4,7};
            foreach( var i in input)
            {
                var outputs = i.Split('|').Last().Split(" ");
                digitCount += outputs.Count(o => segmentCounts.Contains(o.Count()) );
            }
            return digitCount;
        }

        static int SecondMethod(string[] input)
        {
            var outValue = 0;
            foreach(var i in input)
            {
                var numbers = i.Split('|').First().Split(" ",StringSplitOptions.RemoveEmptyEntries);
                var outputs = i.Split('|').Last().Split(" ",StringSplitOptions.RemoveEmptyEntries);

                var numberMap = new Dictionary<int, string>();

                numberMap[1] = numbers.First(n => n.Count() == 2);
                numberMap[4] = numbers.First(n => n.Count() == 4);
                numberMap[7] = numbers.First(n => n.Count() == 3);
                numberMap[8] = numbers.First(n => n.Count() == 7);

                var fiveSegments = numbers.Where(n => n.Count() == 5).ToList();
                var sixSegments = numbers.Where(n => n.Count() == 6).ToList();

                // Three is the only 5-seg with both segs from 1
                numberMap[3] = fiveSegments.First(f => f.Contains(numberMap[1][0]) && f.Contains(numberMap[1][1]));
                fiveSegments.Remove(numberMap[3]);

                // Nine is the only 6-seg with all segs from 3
                numberMap[9] = sixSegments.First(f => f.Contains(numberMap[3][0]) && f.Contains(numberMap[3][1]) && f.Contains(numberMap[3][2]) && f.Contains(numberMap[3][3]) && f.Contains(numberMap[3][4]));
                sixSegments.Remove(numberMap[9]);

                // Zero is the remaining 6-seg with both segs from 1
                numberMap[0] = sixSegments.First(f => f.Contains(numberMap[1][0]) && f.Contains(numberMap[1][1]));
                sixSegments.Remove(numberMap[0]);

                // Six is the only 6-seg with both segs from 1
                numberMap[6] = sixSegments.First();
                sixSegments.Remove(numberMap[6]);

                // Five is the only remaining 5-seg that is a subset of 9
                numberMap[5] = fiveSegments.First(f => f.ToCharArray().Intersect(numberMap[9].ToCharArray()).Count() == 5);
                fiveSegments.Remove(numberMap[5]);

                // Two is the only remaining 5-seg
                numberMap[2] = fiveSegments.First();


                string outString = string.Empty;
                foreach(var o in outputs)
                {
                    outString += numberMap.FirstOrDefault(n => string.Concat(n.Value.OrderBy(c => c)) == string.Concat(o.OrderBy(c => c))).Key;
                }
                outValue += int.Parse(outString);
            }

            return outValue;
        }
    }
}
