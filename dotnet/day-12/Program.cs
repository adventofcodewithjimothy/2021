﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day12
{
    class Program
    {
        static void Main(string[] args)
        {
            var testInput = System.IO.File.ReadAllLines("inputs/test");
            var testInput2 = System.IO.File.ReadAllLines("inputs/test2");
            var testInput3 = System.IO.File.ReadAllLines("inputs/test3");
            var input = System.IO.File.ReadAllLines("inputs/input");

            var testMap = new Map(testInput);
            var testMap2 = new Map(testInput2);
            var testMap3 = new Map(testInput3);
            var map = new Map(input);

            Console.WriteLine($"Phase 1, Test Input 1: {testMap.Paths("start", new List<string>())}");
            // Console.WriteLine($"Phase 1, Test Input 2: {testMap2.Paths("start", new List<string>())}");
            // Console.WriteLine($"Phase 1, Test Input 3: {testMap3.Paths("start", new List<string>())}");
            // Console.WriteLine($"Phase 1, Real input: {map.Paths("start", new List<string>())}");

            Console.WriteLine($"Phase 2, Test Input 1: {testMap.Paths("start", new List<string>(), true)}");
            Console.WriteLine($"Phase 2, Test Input 2: {testMap2.Paths("start", new List<string>(), true)}");
            Console.WriteLine($"Phase 2, Test Input 3: {testMap3.Paths("start", new List<string>(), true)}");
            Console.WriteLine($"Phase 2, Real input: {map.Paths("start", new List<string>(), true)}");
        }

        public class Map
        {
            public List<Node> Nodes { get; set; }

            public Map(string[] input)
            {
                Nodes = new List<Node>();
                foreach (var i in input)
                {
                    var link = i.Split("-");

                    var first = Nodes.FirstOrDefault(n => n.Name == link.First());
                    if (first is null)
                    {
                        Nodes.Add(new Node
                        {
                            Name = link.First(),
                            NodeLinks = new List<string> { link.Last() },
                            IsLarge = link.First().Any(char.IsUpper),
                        });
                    }
                    else
                    {
                        first.NodeLinks.Add(link.Last());
                    }

                    var last = Nodes.FirstOrDefault(n => n.Name == link.Last());
                    if (last is null)
                    {
                        Nodes.Add(new Node
                        {
                            Name = link.Last(),
                            NodeLinks = new List<string> { link.First() },
                            IsLarge = link.Last().Any(char.IsUpper),
                        });
                    }
                    else
                    {
                        last.NodeLinks.Add(link.First());
                    }
                }
            }

            public int Paths(string nodeName, List<string> history = null, bool canRevisit = false)
            {
                var paths = 0;
                if(nodeName == "end")
                {
                    // foreach( var h in history)
                    // {
                    //     Console.Write($"{h} - ");
                    // }
                    // Console.WriteLine();
                    return 1;
                }

                foreach(var node in Nodes.Single(n => n.Name == nodeName).NodeLinks)
                {
                    if(node == "start")
                    {
                        continue;
                    }
                    var canVisit = canRevisit || !history.Any(h => h == node) || Nodes.Any(n => n.Name == node && n.IsLarge);


                    if(canVisit)
                    {
                        var newCanRevisit = history.Any(h => h == node) && Nodes.Any(n => n.Name == node && !n.IsLarge) ? false : canRevisit;
                        var newHistory = new List<string>(history);
                        newHistory.Add(nodeName);
                        paths += Paths(node, newHistory, newCanRevisit);
                    }
                }
                return paths;
            }
        }

        public class Node
        {
            public string Name { get; set; }
            public bool IsLarge { get; set; }
            public List<string> NodeLinks { get; set; }

            public Node()
            {
                NodeLinks = new List<string>();
            }
        }
    }
}
