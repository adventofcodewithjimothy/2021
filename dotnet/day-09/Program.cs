﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day09
{
    class Program
    {
        static void Main(string[] args)
        {
            var testInput = System.IO.File.ReadAllLines("inputs/test");
            var input = System.IO.File.ReadAllLines("inputs/input");

            Console.WriteLine($"Stage 1: Test Input {FirstMethod(testInput)}");
            Console.WriteLine($"Stage 1: Real Input {FirstMethod(input)}");

            Console.WriteLine($"Stage 2: Test Input {SecondMethod(testInput)}");
            Console.WriteLine($"Stage 2: Real Input {SecondMethod(input)}");
        }

        static int FirstMethod(string[] input)
        {
            var map = input.Select(i => i.ToCharArray().Select(x => x - '0').ToList()).ToList();
            var lowCount = 0;

            for(var i = 0; i < map.Count(); i++)
            {
                for(var j = 0; j < map[i].Count(); j++)
                {
                    var x = map[i][j];

                    var isTop = i == 0;
                    var isLeft = j == 0;
                    var isBottom = i == map.Count() - 1;
                    var isRight = j == map[i].Count() - 1;

                    if( !isTop && x >= map[i-1][j]
                        || !isBottom && x >= map[i + 1][j]
                        || !isLeft && x >= map[i][j - 1]
                        || !isRight && x >= map[i][j + 1])
                    {
                        continue;
                    }
                    lowCount += x + 1;
                }
            }

            return lowCount;
        }

        static int SecondMethod(string[] input)
        {
            var map = input.Select(i => i.ToCharArray().Select(x => x - '0').ToList()).ToList();

            var basins = new List<int>();

            for(var i = 0; i < map.Count(); i++)
            {
                for(var j = 0; j < map[i].Count(); j++)
                {
                    var x = map[i][j];

                    var isTop = i == 0;
                    var isLeft = j == 0;
                    var isBottom = i == map.Count() - 1;
                    var isRight = j == map[i].Count() - 1;

                    if( !isTop && x >= map[i-1][j]
                        || !isBottom && x >= map[i + 1][j]
                        || !isLeft && x >= map[i][j - 1]
                        || !isRight && x >= map[i][j + 1])
                    {
                        continue;
                    }
                    basins.Add(BasinSize(i, j, map));
                }
            }
            var largest = basins.OrderByDescending(x => x).Take(3).ToArray();

            return largest[0] * largest[1] * largest[2];
        }

        static int BasinSize(int x, int y, List<List<int>> map)
        {
            var basinCount = 1;

            var thisValue = map[x][y];

            if(x > 0 && thisValue < map[x - 1][y] && map[x-1][y] < 9)
            {
                basinCount += BasinSize(x-1,y,map);
            }
            if(y > 0 && thisValue < map[x][y-1] && map[x][y-1] < 9)
            {
                basinCount += BasinSize(x,y-1,map);
            }
            if(x < map.Count() - 1 && thisValue < map[x + 1][y] && map[x+1][y] < 9)
            {
                basinCount += BasinSize(x+1,y,map);
            }
            if(y < map[x].Count() - 1 && thisValue < map[x][y+1] && map[x][y+1] < 9)
            {
                basinCount += BasinSize(x,y+1,map);
            }
            map[x][y] = 9;

            return basinCount;
        }
    }
}
