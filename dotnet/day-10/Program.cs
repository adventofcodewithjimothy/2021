﻿using System;
using System.Text.RegularExpressions;
using System.Linq;
using System.Collections.Generic;

namespace AdventOfCode2021.Day10
{
    class Program
    {
        static void Main(string[] args)
        {
            var testInput = System.IO.File.ReadAllLines("inputs/test");
            var input = System.IO.File.ReadAllLines("inputs/input");

            Console.WriteLine($"Stage 1: Test Input {FirstMethod(testInput)}");
            Console.WriteLine($"Stage 1: Real Input {FirstMethod(input)}");

            Console.WriteLine($"Stage 2: Test Input {SecondMethod(testInput)}");
            Console.WriteLine($"Stage 2: Real Input {SecondMethod(input)}");
        }

        static int FirstMethod(string[] input)
        {
            var openEOL = new Regex(@"[\(\{\[\<]$");
            var invalidPar   = new Regex(@"\(([\]\}\>])");
            var invalidBrack = new Regex(@"\[([\)\}\>])");
            var invalidSquig = new Regex(@"\{([\)\]\>])");
            var invalidAngle = new Regex(@"\<([\)\]\}])");

            var charScores = new Dictionary<string, int>
            {
                {")", 3},
                {"]", 57},
                {"}", 1197},
                {">", 25137}
            };

            int score = 0;
            foreach(var line in input)
            {
                var i = line;
                while(i.Length > 0)
                {
                    var parMatches = invalidPar.Matches(i);
                    if(parMatches.Count > 0)
                    {
                        score += charScores[parMatches.First().Groups[1].Value];
                        break;
                    }
                    var brackMatches = invalidBrack.Matches(i);
                    if(brackMatches.Count > 0)
                    {
                        score += charScores[brackMatches.First().Groups[1].Value];
                        break;
                    }
                    var squigMatches = invalidSquig.Matches(i);
                    if(squigMatches.Count > 0)
                    {
                        score += charScores[squigMatches.First().Groups[1].Value];
                        break;
                    }
                    var angleMatches = invalidAngle.Matches(i);
                    if(angleMatches.Count > 0)
                    {
                        score += charScores[angleMatches.First().Groups[1].Value];
                        break;
                    }


                    i = i.Replace("()", "");
                    i = i.Replace("[]", "");
                    i = i.Replace("{}", "");
                    i = i.Replace("<>", "");
                    i = openEOL.Replace(i, "");
                }
            }

            return score;
        }

        static long SecondMethod(string[] input)
        {
            var openEOL = new Regex(@"[\(\{\[\<]$");
            var invalidPar   = new Regex(@"\(([\]\}\>])");
            var invalidBrack = new Regex(@"\[([\)\}\>])");
            var invalidSquig = new Regex(@"\{([\)\]\>])");
            var invalidAngle = new Regex(@"\<([\)\]\}])");

            var charScores = new Dictionary<char, int>
            {
                {'(', 1},
                {'[', 2},
                {'{', 3},
                {'<', 4}
            };

            var scores = new List<long>();
            foreach(var line in input)
            {
                long score = 0;
                var i = line;
                while(i.Length > 0)
                {
                    var parMatches = invalidPar.Matches(i);
                    if(parMatches.Count > 0)
                    {
                        break;
                    }
                    var brackMatches = invalidBrack.Matches(i);
                    if(brackMatches.Count > 0)
                    {
                        break;
                    }
                    var squigMatches = invalidSquig.Matches(i);
                    if(squigMatches.Count > 0)
                    {
                        break;
                    }
                    var angleMatches = invalidAngle.Matches(i);
                    if(angleMatches.Count > 0)
                    {
                        break;
                    }

                    i = i.Replace("()", "");
                    i = i.Replace("[]", "");
                    i = i.Replace("{}", "");
                    i = i.Replace("<>", "");

                    if(openEOL.IsMatch(i))
                    {
                        score = score * 5 + charScores[i.ToCharArray().Last()];
                        i = openEOL.Replace(i, "");
                    }
                    if(i.Length == 0)
                    {
                        scores.Add(score);
                    }
                }
            }

            return scores.OrderBy(s => s).Skip((scores.Count() - 1) / 2).First();
        }
    }
}
