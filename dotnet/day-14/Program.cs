﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day14
{
    class Program
    {
        static void Main(string[] args)
        {
            int depth = 10;
            if(args.Length > 0)
            {
                depth = int.Parse(args[0]);
            }
            var testInput = System.IO.File.ReadAllLines("inputs/test");
            var input = System.IO.File.ReadAllLines("inputs/input");

            var testPolymer = new Polymer(testInput);
            var polymer = new Polymer(input);
            
            Console.WriteLine(testPolymer.GetTheDifference(depth));
            Console.WriteLine(polymer.GetTheDifference(depth));
        }

        public class Polymer
        {
            public string InitialState { get; set; }

            public Dictionary<string, string> Reactions { get; set; }

            public Dictionary<(string, int), Dictionary<char, long>> OhDearGodWhatHaveIDone { get; set; }

            public Polymer(string[] input)
            {
                InitialState = input.First();

                Reactions = new Dictionary<string, string>();
                OhDearGodWhatHaveIDone = new Dictionary<(string, int), Dictionary<char, long>>();
                foreach(var i in input.Skip(2))
                {
                    var componennts = i.Split(" -> ");
                    var key = componennts.First();
                    Reactions.Add(key, $"{key[0]}{componennts.Last()}{key[1]}");
                }
            }

            public long GetTheDifference(int depth)
            {
                for(var i = 0; i < InitialState.Length - 1; i++)
                {
                    Replace(InitialState.Substring(i, 2), depth);
                }

                foreach(var ohGod in OhDearGodWhatHaveIDone.Where(x => x.Key.Item2 == depth))
                {
                    Console.WriteLine($"{ohGod.Key} : {ohGod.Value}");
                    foreach(var oh in ohGod.Value)
                    {
                        Console.WriteLine($"{oh.Key} : {oh.Value}");
                    }
                    Console.WriteLine(ohGod.Value.Sum(x => x.Value));
                }

                var counts = GetLetterCounts(depth);

                counts[InitialState.ToCharArray().Last()] += 1;

                foreach(var c in counts)
                {
                    Console.WriteLine($"{c.Key} - {c.Value}");
                }
                Console.WriteLine(counts.Sum(x => x.Value));
                return counts.Max(c => c.Value) - counts.Min(c => c.Value);
            }

            public void Replace(string input, int depth)
            {
                if(OhDearGodWhatHaveIDone.ContainsKey((input, depth)))
                {
                    return;
                }

                if(depth == 1)
                {
                    var value = Reactions[input].Substring(0,2).GroupBy(x => x).ToDictionary(k => k.Key, v => (long)v.Count());
                    OhDearGodWhatHaveIDone.Add((input, depth), value);
                    return;
                }
                
                Replace(Reactions[input].Substring(0,2), depth - 1);
                Replace(Reactions[input].Substring(1,2), depth - 1);
                OhDearGodWhatHaveIDone[(input, depth)] = CombineEntries(input, depth);
            }

            public Dictionary<char, long> CombineEntries(string input, int depth)
            {
                var value = new Dictionary<char, long>(OhDearGodWhatHaveIDone[(Reactions[input].Substring(0,2), depth - 1)]);

                foreach( var c in OhDearGodWhatHaveIDone[(Reactions[input].Substring(1,2), depth - 1)])
                {
                    if(value.ContainsKey(c.Key))
                    {
                        value[c.Key] += c.Value;
                    }
                    else
                    {
                        value.Add(c.Key, c.Value);
                    }
                }

                return value;
            }

            public Dictionary<char, long> GetLetterCounts(int depth)
            {
                var counts = new Dictionary<char, long>();
                foreach(var entry in OhDearGodWhatHaveIDone.Where(x => x.Key.Item2 == depth))
                {
                    var multiplier = entry.Key.Item1 == "CO" || entry.Key.Item1 == "PV" ? 2 : 1;
                    foreach(var e in entry.Value)
                    {
                        if(counts.ContainsKey(e.Key))
                        {
                            counts[e.Key] += e.Value * multiplier;
                        }
                        else
                        {
                            counts.Add(e.Key, e.Value * multiplier);
                        }
                    }
                }

                
                return counts;
            }
        }
    }
}
