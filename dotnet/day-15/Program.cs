﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day15
{
    class Program
    {
        static void Main(string[] args)
        {
            var testInput = System.IO.File.ReadAllLines("inputs/test");
            var input = System.IO.File.ReadAllLines("inputs/input");

            var testMap = new Map(testInput);
            Console.WriteLine(testMap.GetRisk((0,0), (9,9)));
            var map = new Map(input);
            Console.WriteLine(map.GetRisk((0,0),(99,99)));

            var testTiledMap = new TiledMap(testInput);
            Console.WriteLine(testTiledMap.GetRisk((0,0), (49,49)));
            var tiledMap = new TiledMap(input);
            Console.WriteLine(tiledMap.GetRisk((0,0),(499,499)));
        }

        public class TiledMap : Map
        {
            public TiledMap(string[] input) : base(input)
            {
                Vertices = new List<Vertex>();

                for(int i = 0; i < 5; i++)
                {
                    for(int j = 0; j < 5; j++)
                    {
                        int y = 0;
                        foreach( var line in input)
                        {
                            int x = 0;
                            foreach(var c in line.ToCharArray())
                            {
                                Vertices.Add(new Vertex(x + input.First().Length * i, y + input.Length * j, (i + j + (long)char.GetNumericValue(c)) > 9 ? (i + j + (long)char.GetNumericValue(c)) - 9: (i + j + (long)char.GetNumericValue(c))));
                                x++;
                            }
                            y++;
                        }
                    }
                }

                // for(int i = 0; i < 50; i++)
                // {
                //     for(int j = 0; j < 50; j++)
                //     {
                //         var thisOne = Vertices.First(x=> x.X == j && x.Y == i);
                //         Console.Write(thisOne.Risk);
                //     }
                //     Console.WriteLine();
                // }
            }
        }

        public class Map
        {
            public List<Vertex> Vertices { get; set; }

            public int MaxX => Vertices.Max(v => v.X);
            public int MaxY => Vertices.Max(v => v.Y);
            public int MinX => Vertices.Min(v => v.X);
            public int MinY => Vertices.Min(v => v.Y);

            public Map(string[] input)
            {
                Vertices = new List<Vertex>();
                int y = 0;
                foreach( var line in input)
                {
                    int x = 0;
                    foreach(var c in line.ToCharArray())
                    {
                        Vertices.Add(new Vertex(x, y, (long)char.GetNumericValue(c)));
                        x++;
                    }
                    y++;
                }
            }

            public long GetRisk((int, int) start, (int,int) end)
            {
                var startingPoint = Vertices.First(v => v.X == start.Item1 && v.Y == start.Item2);
                startingPoint.Risk = 0;
                startingPoint.TentativeRisk = 0;

                var visited = 0;

                while(!Vertices.First(v => v.X == end.Item1 && v.Y == end.Item2).Visited)
                {
                    var current = Vertices.Where(v => !v.Visited).OrderBy(v => v.TentativeRisk).First();
                    current.Visited = true;
                    visited++;

                    var right = Vertices.FirstOrDefault(v => v.X == current.X + 1 && v.Y == current.Y);
                    if(right != null)
                    {
                        right.TentativeRisk = right.TentativeRisk > current.TentativeRisk + right.Risk ? current.TentativeRisk + right.Risk : right.TentativeRisk;
                    }
                    var down = Vertices.FirstOrDefault(v => v.X == current.X && v.Y == current.Y + 1);
                     if(down != null)
                    {
                        down.TentativeRisk = down.TentativeRisk > current.TentativeRisk + down.Risk ? current.TentativeRisk + down.Risk : down.TentativeRisk;
                    }
                    var left = Vertices.FirstOrDefault(v => v.X == current.X - 1 && v.Y == current.Y);
                     if(left != null)
                    {
                        left.TentativeRisk = left.TentativeRisk > current.TentativeRisk + left.Risk ? current.TentativeRisk + left.Risk : left.TentativeRisk;
                    }
                    var up = Vertices.FirstOrDefault(v => v.X == current.X && v.Y == current.Y - 1);
                     if(up != null)
                    {
                        up.TentativeRisk = up.TentativeRisk > current.TentativeRisk + up.Risk ? current.TentativeRisk + up.Risk : up.TentativeRisk;
                    }
                    if(visited  % 1000 == 0)
                    {
                        Console.WriteLine(visited);
                    }
                }

                return Vertices.First(v => v.X == end.Item1 && v.Y == end.Item2).TentativeRisk;
            }
        }

        public class Vertex
        {
            public bool Visited { get; set; }
            public long TentativeRisk { get; set; }
            public long Risk { get; set; }
            public int X { get; set; }
            public int Y { get; set; }

            public Vertex(int x, int y, long risk)
            {
                Visited = false;
                TentativeRisk = long.MaxValue;
                X = x;
                Y = y;
                Risk = risk;
            }
        }
    }
}
