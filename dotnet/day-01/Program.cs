﻿using System;
using System.Linq;

namespace AdventOfCode2021.Day01
{
    class Program
    {
        static void Main(string[] args)
        {
            var testInput = System.IO.File.ReadAllLines("inputs/test").Select(i => int.Parse(i)).ToArray();
            var input = System.IO.File.ReadAllLines("inputs/input").Select(i => int.Parse(i)).ToArray();

            Console.WriteLine($"Stage 1: Test Input {FinalMethod(testInput, 1)}");
            Console.WriteLine($"Stage 1: Real Input {FinalMethod(input, 1)}");
            Console.WriteLine($"Stage 2: Test Input {FinalMethod(testInput, 3)}");
            Console.WriteLine($"Stage 2: Real Input {FinalMethod(input, 3)}");
        }

        static int FinalMethod(int[] measurements, int windowSize)
        {
            int lastSet = int.MaxValue;
            int increasedCount = 0;
            for(int i = windowSize - 1; i < measurements.Count(); i++)
            {
                var thisSet = 0;
                for(int j = 0; j <= windowSize - 1; j++)
                {
                    thisSet += measurements[i - j];
                }

                if(thisSet > lastSet)
                {
                    increasedCount++;
                }
                lastSet = thisSet;
            }

            return increasedCount;
        }
    }
}
