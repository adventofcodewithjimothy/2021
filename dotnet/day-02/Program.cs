﻿using System;
using System.Linq;

namespace AdventOfCode2021.Day02
{
    class Program
    {
        static void Main(string[] args)
        {
            var testInput = System.IO.File.ReadAllLines("inputs/test");
            var input = System.IO.File.ReadAllLines("inputs/input");

            Console.WriteLine($"Stage 1: Test Input {FirstMethod(testInput)}");
            Console.WriteLine($"Stage 1: Real Input {FirstMethod(input)}");
            Console.WriteLine($"Stage 2: Test Input {SecondMethod(testInput)}");
            Console.WriteLine($"Stage 2: Real Input {SecondMethod(input)}");
        }

        static int FirstMethod(string[] input)
        {
            var h = 0;
            var d = 0;
            foreach( var i in input)
            {
                var c = i.Split(" ");
                var distance = int.Parse(c[1]);
                switch(c[0])
                {
                    case "up":
                        d -= distance;
                        break;
                    case "down":
                        d += distance;
                        break;
                    case "forward":
                        h += distance;
                        break;
                }
            }

            return h * d;
        }

        static int SecondMethod(string[] input)
        {
            var h = 0;
            var d = 0;
            var aim = 0;
            foreach( var i in input)
            {
                var c = i.Split(" ");
                var distance = int.Parse(c[1]);
                switch(c[0])
                {
                    case "up":
                        aim -= distance;
                        break;
                    case "down":
                        aim += distance;
                        break;
                    case "forward":
                        h += distance;
                        d += aim * distance;
                        break;
                }
            }

            return h * d;
        }
    }
}
