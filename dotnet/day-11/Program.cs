﻿using System;
using System.Text.RegularExpressions;
using System.Linq;
using System.Collections.Generic;

namespace AdventOfCode2021.Day11
{
    class Program
    {
        static void Main(string[] args)
        {
            var testInput = System.IO.File.ReadAllLines("inputs/test");
            var input = System.IO.File.ReadAllLines("inputs/input");

            Console.WriteLine($"Stage 1: Test Input (10 steps) {FirstMethod(testInput, 10)}");
            Console.WriteLine($"Stage 1: Test Input (100 steps) {FirstMethod(testInput, 100)}");
            Console.WriteLine($"Stage 1: Real Input {FirstMethod(input, 100)}");

            Console.WriteLine($"Stage 2: Test Input {SecondMethod(testInput)}");
            Console.WriteLine($"Stage 2: Real Input {SecondMethod(input)}");
        }

        static int FirstMethod(string[] input, int steps)
        {
            var flashes = 0;

            var map = new Map(input);

            for(int s = 0; s < steps; s++)
            {
                flashes += map.Step();
            }

            return flashes;
        }

        static long SecondMethod(string[] input)
        {
            var steps = 0;
            var map = new Map(input);
            while(true)
            {
                steps++;
                if(map.Step() == input.Count() * input.First().Count())
                {
                    return steps;
                }
            }
        }

        public class Map
        {
            public List<List<Octopus>> Octopoda { get; set;}

            public Map(string[] input)
            {
                Octopoda = new List<List<Octopus>>();

                for(int i = 0; i < input.Length; i++)
                {
                    Octopoda.Add(new List<Octopus>());
                    for(int j = 0; j < input.First().Length; j++)
                    {
                        Octopoda[i].Add(new Octopus(int.Parse(input[i].ToCharArray()[j].ToString())));
                    }
                }
            }

            public int Step()
            {
                for(var i = 0; i < Octopoda.Count(); i++)
                {
                    for(var j = 0; j < Octopoda.First().Count(); j++)
                    {
                        Bump(i,j);
                    }
                }

                int flashes = Octopoda.Sum(x => x.Count(y => y.Flashed));
                Octopoda.ForEach(x => x.ForEach(y => y.Reset()));
                return flashes;
            }

            public void Bump(int x, int y)
            {
                if(x < 0 || y < 0 || x >= Octopoda.Count() || y >= Octopoda.First().Count())
                {
                    return;
                }
                if(Octopoda[x][y].Increase())
                {
                    for(int i = -1; i <= 1; i++)
                    {
                        for(int j = -1; j <= 1; j++)
                        {
                            if(i != 0 || j != 0)
                            {
                                Bump(x + i,y + j);
                            }
                        }
                    }
                }
            }
        }

        public class Octopus
        {
            public int Energy { get; set; }
            public bool Flashed { get; set; }

            public Octopus(int energy)
            {
                Energy = energy;
                Flashed = false;
            }

            public bool Increase()
            {
                if(Flashed)
                {
                    return false;
                }
                Energy++;
                if(Energy > 9 && !Flashed)
                {
                    Energy = 0;
                    Flashed = true;
                    return true;
                }
                return false;
            }

            public void Reset()
            {
                if(Flashed)
                {
                    Energy = 0;
                    Flashed = false;
                }
            }
        }
    }
}
