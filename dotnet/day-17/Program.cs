﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day17
{
    class Program
    {
        static void Main(string[] args)
        {
            var testInput = System.IO.File.ReadAllLines("inputs/test");
            var input = System.IO.File.ReadAllLines("inputs/input");      

            var testTarget = new Target
            {
                XMin = 20,
                XMax = 30,
                YMin = -10,
                YMax = -5
            };
            var target = new Target
            {
                XMin = 248,
                XMax = 285,
                YMin = -85,
                YMax = -56
            };

            var testResult = testTarget.TakeAllTheShots();
            var result = target.TakeAllTheShots();

            Console.WriteLine($"{testResult.Item1}, {testResult.Item2}");
            Console.WriteLine($"{result.Item1}, {result.Item2}");
        }

        public class Target
        {
            public int XMin { get; set; }
            public int XMax { get; set; }
            public int YMin { get; set; }
            public int YMax { get; set; }

            public int X { get; set; } = 0;
            public int Y { get; set; } = 0;

            public (int, int) TakeAllTheShots()
            {
                var maxHeight = 0;
                var validShots = 0;
                for(var x = 1; x <= XMax; x++)
                {
                    for( var y = YMin; y <=2000; y++)
                    {
                        var result = TakeTheShot(x, y);
                        if(result.HasValue)
                        {
                            validShots++;
                        }
                                                
                        maxHeight = Math.Max(maxHeight, result ?? 0);
                        // Console.WriteLine(maxHeight);
                    }
                }
                return (maxHeight, validShots);
            }

            public int? TakeTheShot(int dX, int dY)
            {
                var maxY = Y;
                while( X <= XMax && Y >= YMin)
                {
                    // Console.WriteLine($"{X}, {Y}");
                    X += dX;
                    Y += dY;
                    dY--;
                    if( dX != 0 )
                    {
                        dX--;
                    }
                    maxY = Math.Max(Y, maxY);
                    if( X >= XMin && X <= XMax && Y >= YMin && Y <= YMax)
                    {
                        X = 0;
                        Y = 0;
                        return maxY;
                    }
                }
                X = 0; Y = 0;
                return null;
            }
        }
    }
}
