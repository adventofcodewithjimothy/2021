﻿using System;
using System.Linq;

namespace AdventOfCode2021.Day07
{
    class Program
    {
        static void Main(string[] args)
        {
            var testInput = System.IO.File.ReadAllLines("inputs/test").First().Split(",").Select(i => int.Parse(i)).ToArray();
            var input = System.IO.File.ReadAllLines("inputs/input").First().Split(",").Select(i => int.Parse(i)).ToArray();

            Console.WriteLine($"Stage 1: Test Input {FirstMethod(testInput)}");
            Console.WriteLine($"Stage 1: Real Input {FirstMethod(input)}");
            Console.WriteLine($"Stage 2: Test Input {SecondMethod(testInput)}");
            Console.WriteLine($"Stage 2: Real Input {SecondMethod(input)}");
        }

        static int FirstMethod(int[] input)
        {
            var max = input.Max();

            int leastFuel = int.MaxValue;

            for(var p = 0; p <= max; p++)
            {
                var fuel = 0;
                foreach(var i in input)
                {
                    fuel += Math.Abs(p - i);
                }
                leastFuel = fuel < leastFuel ? fuel : leastFuel;
            }
            return leastFuel;
        }

        static int SecondMethod(int[] input)
        {
            var max = input.Max();

            int leastFuel = int.MaxValue;

            for(var p = 0; p <= max; p++)
            {
                var fuel = 0;
                foreach(var i in input)
                {
                    var distance = Math.Abs(p - i);
                    fuel += (distance + 1) * distance / 2;
                }
                leastFuel = fuel < leastFuel ? fuel : leastFuel;
            }
            return leastFuel;
        }
    }
}
