﻿using System;
using System.Linq;

namespace AdventOfCode2021.Day03
{
    class Program
    {
        static void Main(string[] args)
        {
            var testInput = System.IO.File.ReadAllLines("inputs/test");
            var input = System.IO.File.ReadAllLines("inputs/input");

            Console.WriteLine($"Stage 1 (Test Input) {FirstMethod(testInput)}");
            Console.WriteLine($"Stage 1 (Real Input) {FirstMethod(input)}");
            Console.WriteLine($"Stage 2 (Test Input) {SecondMethod(testInput)}");
            Console.WriteLine($"Stage 2 (Real Input) {SecondMethod(input)}");
        }

        static int FirstMethod(string[] input)
        {
            var length = input.First().Length;

            string gamma = string.Empty;
            string epsilon = string.Empty;

            for(var i = 0; i < length; i++)
            {
                var zeroCount = 0;
                var oneCount = 0;
                foreach(var e in input)
                {
                    var _ = e[i] == '0' ? zeroCount++ : oneCount++;
                }

                gamma += zeroCount > oneCount ? '0' : '1';
                epsilon += zeroCount < oneCount ? '0' : '1';
            }

            var gammaNum = Convert.ToInt32(gamma, 2);
            var epsilonNum = Convert.ToInt32(epsilon, 2);
            return gammaNum * epsilonNum;
        }

        static int SecondMethod(string[] input)
        {
            return Convert.ToInt32(Oxygen(input),2) * Convert.ToInt32(CarbonDioxide(input),2);
        }

        static string Oxygen(string[] input)
        {
            if(input.Count() == 1)
            {
                return input[0];
            }

            if(input.Sum(x => int.Parse(x[0].ToString())) >= (float)input.Count() / 2)
            {
                return $"1{Oxygen(input.Where(x => x[0] == '1').Select(x => x.Substring(1)).ToArray())}";
            }
            return $"0{Oxygen(input.Where(x => x[0] == '0').Select(x => x.Substring(1)).ToArray())}";
        }

        static string CarbonDioxide(string[] input)
        {
            if(input.Count() == 1)
            {
                return input[0];
            }

            if(input.Sum(x => int.Parse(x[0].ToString())) < (float)input.Count() / 2)
            {
                return $"1{CarbonDioxide(input.Where(x => x[0] == '1').Select(x => x.Substring(1)).ToArray())}";
            }
            return $"0{CarbonDioxide(input.Where(x => x[0] == '0').Select(x => x.Substring(1)).ToArray())}";
        }
    }
}
