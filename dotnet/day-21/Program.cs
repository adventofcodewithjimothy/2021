﻿using System;
using System.Linq;

namespace AdventOfCode2021.Day21
{
    class Program
    {
        static void Main(string[] args)
        {
            var testGame = new Game
            {
                Player1Position = 4,
                Player2Position = 8,
                Die = new Deterministic(),
            };

            var game = new Game
            {
                Player1Position = 10,
                Player2Position = 7,
                Die = new Deterministic(),
            };

            var testResult = testGame.PlayToWin();

            Console.WriteLine($"{testResult.Item1} - {testResult.Item2} - {testResult.Item1 * testResult.Item2}");

            testResult = game.PlayToWin();

            Console.WriteLine($"{testResult.Item1} - {testResult.Item2} - {testResult.Item1 * testResult.Item2}");

            var testDirac = new Game
            {
                Player1Position = 4,
                Player2Position = 8,
                Die = new Quantum(),
                Goal = 21,
            };

            testResult = testGame.PlayToWin();

            Console.WriteLine($"{testResult.Item1} - {testResult.Item2} - {testResult.Item1 * testResult.Item2}");

        }

        public class Game
        {
            public int Player1Position { get; set; }
            public int Player1Score { get; set; } = 0;
            public int Player2Position { get; set; }
            public int Player2Score { get; set; } = 0;

            public int Goal { get; set; } = 1000;

            public Die Die { get; set; }

            private int RollCount = 0;

            public (int, int) PlayToWin()
            {
                while(true)
                {
                    Player1Position += Roll();
                    if(Player1Position % 10 != 0)
                    {
                        Player1Position %= 10;
                    }
                    else
                    {
                        Player1Position = 10;
                    }
                    Player1Score += Player1Position;
                    if(Player1Score >= 1000)
                    {
                        return (Player2Score, RollCount);
                    }

                    Player2Position += Roll();
                    if(Player2Position % 10 != 0)
                    {
                        Player2Position %= 10;
                    }
                    else
                    {
                        Player2Position = 10;
                    }
                    Player2Score += Player2Position;
                    if(Player2Score >= 1000)
                    {
                        return (Player1Score, RollCount);
                    }
                }
            }

            public int Roll()
            {
                RollCount += 3;
                return Die.Roll() + Die.Roll() + Die.Roll();
            }
        }

        public abstract class Die
        {
            protected int State;

            public abstract int Roll();
        }

        public class Deterministic : Die
        {
            public Deterministic()
            {
                State = 1;
            }

            public override int Roll()
            {
                if(State > 100)
                {
                    State = 1;
                }
                return State++;
            }
        }

        public class Quantum : Die
        {
            public override int Roll()
            {
                throw new NotImplementedException();
            }
        }
    }
}
