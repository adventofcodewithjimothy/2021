﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day06
{
    class Program
    {
        static void Main(string[] args)
        {
            var testInput = System.IO.File.ReadAllLines("inputs/test");
            var input = System.IO.File.ReadAllLines("inputs/input");

            Console.WriteLine($"Stage 1: Test Input {FirstMethod(testInput[0],80)}");
            Console.WriteLine($"Stage 1: Real Input {FirstMethod(input[0], 80)}");
            Console.WriteLine($"Stage 1, method 2: Test Input {SecondMethod(testInput[0],80)}");
            Console.WriteLine($"Stage 1, method 2: Real Input {SecondMethod(input[0],80)}");
            Console.WriteLine($"Stage 2, method 2: Test Input {SecondMethod(testInput[0],256)}");
            Console.WriteLine($"Stage 2, method 2: Real Input {SecondMethod(input[0],256)}");
        }

        static int FirstMethod(string input, int days)
        {
            var fish = input.Split(",").Select(f => long.Parse(f)).ToList();

            for(var i = 0; i < days; i++)
            {
                var addCounter = 0;
                var count = fish.Count();
                for(var j = 0; j < count; j++)
                {
                    if(fish[j] == 0)
                    {
                        fish[j] = 6;
                        addCounter++;
                        fish.Add(8);
                    }
                    else
                    {
                        fish[j]--;
                    }
                }
            }
            return fish.Count();
        }

        static long SecondMethod(string input, int days)
        {
            var fish = input.Split(",").Select(f => long.Parse(f)).ToList();

            var daysTilSpawn = new List<long>();

            for(var i = 0; i < 9; i++)
            {
                daysTilSpawn.Add(fish.Count(f => f == i));
            }

            for( var i = 0; i < days; i++)
            {
                var spawns = daysTilSpawn[0];
                daysTilSpawn.RemoveAt(0);
                daysTilSpawn.Add(spawns);
                daysTilSpawn[6] += spawns;
            }
            return daysTilSpawn.Sum();
        }
    }
}
