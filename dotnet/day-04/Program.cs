﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day04
{
    class Program
    {
        static void Main(string[] args)
        {
            var testInput = System.IO.File.ReadAllLines("inputs/test").ToList();
            var input = System.IO.File.ReadAllLines("inputs/input").ToList();

            Console.WriteLine($"Stage 1 (Test Input) {FirstMethod(testInput)}");
            Console.WriteLine($"Stage 1 (Real Input) {FirstMethod(input)}");
            Console.WriteLine($"Stage 2 (Test Input) {SecondMethod(testInput)}");
            Console.WriteLine($"Stage 2 (Real Input) {SecondMethod(input)}");
        }

        static int FirstMethod(List<string> input)
        {
            var plays = input.First().Split(',').Select(i => int.Parse(i));
            var boards = new List<Board>();

            for(var i = 2; i < input.Count(); i += 6)
            {
                boards.Add(new Board(input.Skip(i).Take(5).ToList()));
            }

            var playCount = 0;
            foreach(var play in plays)
            {
                var boardCount = 0;
                playCount++;
                foreach(var board in boards)
                {
                    boardCount++;
                    board.PlayNumber(play);
                    if(board.HasWon())
                    {
                        return board.GetScore() * play;
                    }
                }
            }

            return 1;
        }

        static int SecondMethod(List<string> input)
        {
            var plays = input.First().Split(',').Select(i => int.Parse(i));
            var boards = new List<Board>();

            for(var i = 2; i < input.Count(); i += 6)
            {
                boards.Add(new Board(input.Skip(i).Take(5).ToList()));
            }

            var playCount = 0;
            foreach(var play in plays)
            {
                var boardCount = 0;
                playCount++;
                foreach(var board in boards)
                {
                    boardCount++;
                    board.PlayNumber(play);

                    if(boards.All(b => b.HasWon()))
                    {
                        return board.GetScore() * play;
                    }
                }
            }

            return 1;
        }
    }

    public class Board
    {
        public List<Square> Squares { get; set; }

        public Board(List<string> rows)
        {
            Squares = new List<Square>();
            foreach( var row in rows)
            {
                var squares = row.Split(" ", StringSplitOptions.RemoveEmptyEntries);
                foreach (var s in squares)
                {
                    Squares.Add(new Square(int.Parse(s)));
                }
            }
        }

        public void PlayNumber(int play)
        {
            Squares.FirstOrDefault(s => s.Value == play)?.MarkSquare();
        }

        public bool HasWon()
        {
            // Check rows
            for(int i = 0; i < 25; i += 5)
            {
                if(Squares.Skip(i).Take(5).Count(s => s.Marked) == 5)
                {
                    return true;
                }
            }

            // Check columns because I'm a shitlord
            for(var i = 0; i < 5; i+=1)
            {
                if(Squares[i].Marked && Squares[i+5].Marked && Squares[i+10].Marked && Squares[i+15].Marked && Squares[i+20].Marked)
                {
                    return true;
                }
            }
            return false;
        }

        public int GetScore()
        {
            return Squares.Where(s => !s.Marked).Sum(s => s.Value);
        }
    }

    public class Square
    {
        public int Value { get; set; }
        public bool Marked { get; set; }

        public void MarkSquare()
        {
            Marked = true;
        }

        public Square(int value)
        {
            Value = value;
            Marked = false;
        }
    }
}
