﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day05
{
    class Program
    {
        static void Main(string[] args)
        {
            var testInput = System.IO.File.ReadAllLines("inputs/test");
            var input = System.IO.File.ReadAllLines("inputs/input");

            Console.WriteLine($"Stage 1: Test Input {FirstMethod(testInput)}");
            Console.WriteLine($"Stage 1: Real Input {FirstMethod(input)}");
            Console.WriteLine($"Stage 2: Test Input {SecondMethod(testInput)}");
            Console.WriteLine($"Stage 2: Real Input {SecondMethod(input)}");
        }

        static int FirstMethod(string[] input)
        {
            var coordinates = new Dictionary<(int,int), int>();

            foreach( var i in input)
            {
                var start = i.Split(" -> ").First().Split(",").Select(x => int.Parse(x)).ToArray();
                var end = i.Split(" -> ").Last().Split(",").Select(x => int.Parse(x)).ToArray();

                if(start[0] == end[0] || start[1] == end[1])
                {
                    var horiz = new List<int>{start[0], end[0]}.OrderBy(x => x);
                    var vert = new List<int>{start[1], end[1]}.OrderBy(x => x);
                    for(int x = horiz.First(); x <= horiz.Last(); x++)
                        for(int y = vert.First(); y <= vert.Last(); y++)
                        {
                            if(!coordinates.ContainsKey((x,y)))
                            {
                                coordinates[(x,y)] = 1;
                            }
                            else
                            {
                                coordinates[(x,y)] += 1;
                            }
                        }
                }
            }

            return coordinates.Count(c => c.Value > 1);
        }

        static int SecondMethod(string[] input)
        {
            var coordinates = new Dictionary<(int,int), int>();

            foreach( var i in input)
            {
                var start = i.Split(" -> ").First().Split(",").Select(x => int.Parse(x)).ToArray();
                var end = i.Split(" -> ").Last().Split(",").Select(x => int.Parse(x)).ToArray();

                var horiz = new List<int>{start[0], end[0]}.OrderBy(x => x);
                var vert = new List<int>{start[1], end[1]}.OrderBy(x => x);

                if(start[0] == end[0] || start[1] == end[1])
                {
                    for(int x = horiz.First(); x <= horiz.Last(); x++)
                        for(int y = vert.First(); y <= vert.Last(); y++)
                        {
                            if(!coordinates.ContainsKey((x,y)))
                            {
                                coordinates[(x,y)] = 1;
                            }
                            else
                            {
                                coordinates[(x,y)] += 1;
                            }
                        }
                }
                else
                {
                    var up = start[0] < end[0] && start[1] < end[1] || start[0] > end[0] && start[1] > end[1];
                    if(!up)
                    {
                        vert = vert.OrderByDescending(x => x);
                    }
                    for(var c = (horiz.First(), vert.First()); c.Item1 <= horiz.Last() ; c = (c.Item1 + 1, c.Item2 + (up ? 1 : -1)))
                    {
                        if(!coordinates.ContainsKey((c.Item1,c.Item2)))
                        {
                            coordinates[(c.Item1,c.Item2)] = 1;
                        }
                        else
                        {
                            coordinates[(c.Item1,c.Item2)] += 1;
                        }
                    }
                }
            }

            return coordinates.Count(c => c.Value > 1);
        }
    }
}
