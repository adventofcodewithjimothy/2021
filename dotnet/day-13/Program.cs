﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2021.Day13
{
    class Program
    {
        static void Main(string[] args)
        {
            var testInput = System.IO.File.ReadAllLines("inputs/test");
            var input = System.IO.File.ReadAllLines("inputs/input");

            Console.WriteLine($"Stage 1: Test Input {FirstMethod(testInput)}");
            // Console.WriteLine($"Stage 1: Test Input {FirstMethod(testInput2)}");
            // Console.WriteLine($"Stage 1: Test Input {FirstMethod(testInput3)}");
            Console.WriteLine($"Stage 1: Real Input {FirstMethod(input)}");
            Console.WriteLine($"Stage 2: Test Input {SecondMethod(testInput)}");
            Console.WriteLine($"Stage 2: Real Input {SecondMethod(input)}");
        }

        static int FirstMethod(string[] input)
        {
            var points = input.Where(i => i.Contains(",")).Select(i => (X: int.Parse(i.Split(',')[0]), Y: int.Parse(i.Split(',')[1]))).ToList();

            var folds = input.Where(i => i.Contains('=')).Select(i => (Axis: i.Substring(11).Split('=').First(), Index: int.Parse(i.Substring(11).Split('=').Last())));

            Fold(ref points, folds.First());

            return points.Distinct().Count();
        }

        static int SecondMethod(string[] input)
        {
            var points = input.Where(i => i.Contains(",")).Select(i => (X: int.Parse(i.Split(',')[0]), Y: int.Parse(i.Split(',')[1]))).ToList();

            var folds = input.Where(i => i.Contains('=')).Select(i => (Axis: i.Substring(11).Split('=').First(), Index: int.Parse(i.Substring(11).Split('=').Last())));

            foreach (var fold in folds)
            {
                Fold(ref points, fold);
            }

            for (int y = 0; y <= points.Max(p => p.Y); y++)
            {
                for (int x = 0; x <= points.Max(p => p.X); x++)
                {
                    Console.Write(points.Any(p => p.X == x && p.Y == y) ? "#" : " ");

                }
                Console.WriteLine();
            }
            return points.Distinct().Count();
        }

        public static void Fold(ref List<(int X, int Y)> points, (string Axis, int Index) fold)
        {
            for (int i = 0; i < points.Count(); i++)
            {
                if (fold.Axis == "x")
                {
                    points[i] = (points[i].X < fold.Index ? points[i].X : fold.Index - (points[i].X - fold.Index), points[i].Y);
                }
                else
                {
                    points[i] = (points[i].X, points[i].Y < fold.Index ? points[i].Y : fold.Index - (points[i].Y - fold.Index));
                }
            }
        }

        public class Node
        {
            public string Name { get; set; }
            public bool IsLarge { get; set; }
            public List<string> NodeLinks { get; set; }

            public Node()
            {
                NodeLinks = new List<string>();
            }
        }
    }
}
